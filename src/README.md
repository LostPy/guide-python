<p align="center">
  <a href="https://revealjs.com">
  <img src="https://hakim-static.s3.amazonaws.com/reveal-js/logo/v1/reveal-black-text-sticker.png" alt="reveal.js" width="200">
  </a>
</p>

this slide was created with the [Reveal.js](https://revealjs.com/) framework which is under the  [MIT License](https://github.com/hakimel/reveal.js/blob/master/LICENSE).

reveal.js is an open source HTML presentation framework. It enables anyone with a web browser to create beautiful presentations for free. Check out the live demo at [revealjs.com](https://revealjs.com/).

