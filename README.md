# Bien démarrer avec Python

Ce guide est fait pour tout ceux qui débute en programmation avec Python. Vous trouverez dedans :

 * Les différentes manières de travailler avec Python (des outils simples aux plus avancés)
 * L'installation des outils pour travailler avec ce langage
 * Les notions de bases du langage
 * Des notions de programmation orientée objet
 * Des notions sur la structuration d'un projet en module et package
 * Comment lire la documentation Python et d'autres documentations
 * Comment lire et corriger les erreurs indiqués par l'interpréteur Python
 * Les outils utils à utiliser
 * Pour certaines parties : des liens vers des guides qui approfondissent la notion

## Objectifs du guide

 * Apporter une aide pour commencer à apprendre correctement Python
 * Être simple, général et ne pas rentrer dans les détails des notions présentées
 * Rediriger vers des ressources existantes permettant d'approfondir les notions présentées
 * **Eventuellement** recueillir des ressources classés par catégorie d'utilisation du langage (sciences, interface utilisateur, robotique/embarqué, web...)

## Une suggestion / une erreur repérée ?

Vous pouvez créer une `issue` pour soumettre votre suggestion ou l'erreur repérée.

## Contribuer

Les contributions sont les bienvenus, vous pouvez fork le repository et créer une `merge request`. Veillez tout de même à respecter les [objectifs du projet](#objectifs-du-guide).


## License

Ce guide est sous [license MIT](./LICENSE).
